package com.lixuwei.study.htmlutil.study;

import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;

public class HelloWebRequest {

    public static void main(String[] args) throws IOException {
        WebClient webClient = new WebClient();
//        webClient.getOptions().setCssEnabled();
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());

        String url = "https://www.baidu.com";
        HtmlPage page = webClient.getPage(url);
        System.out.println(page.asXml());
    }
}
