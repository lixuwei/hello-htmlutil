package com.lixuwei.study.htmlutil.web.servlet;

import com.lixuwei.study.htmlutil.Hello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/shop")
public class ShopServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getParameter("name");
        if (url != null && !"".equals(url)) {
            try {
                Hello.parseShop(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        resp.getWriter().write("done!");
        resp.getWriter().flush();
    }
}
