package com.lixuwei.study.htmlutil.web.servlet;

import com.alibaba.fastjson.JSON;
import com.lixuwei.study.htmlutil.Hello;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/getProductId")
public class GetProductIdServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String shopNum = req.getParameter("shopNum");
        String shopListUrl = Hello.getShopListUrl(shopNum);
        resp.setContentType("application/json;charset=UTF-8");
        try {
            List<String> productIds = new ArrayList<>();
            if (StringUtils.isNotEmpty(shopListUrl)) {
                productIds = Hello.parseShop(shopListUrl);
            }
            String jsonString = JSON.toJSONString(productIds);
            resp.getWriter().write(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        resp.getWriter().write(jsonString);
        resp.getWriter().write("ok");
        resp.getWriter().flush();
    }
}
