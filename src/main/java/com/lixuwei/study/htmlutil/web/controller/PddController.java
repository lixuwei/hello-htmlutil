package com.lixuwei.study.htmlutil.web.controller;

import com.lixuwei.study.htmlutil.seleniumhq.HelloSelenium;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Set;

@Controller
public class PddController {

    @RequestMapping("/pdd")
    @ResponseBody
    public String parsePdd() {
        try {
            Set<String> strings = HelloSelenium.parsePdd();
            for (String string : strings) {
                System.out.println(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "ok";
    }

}
