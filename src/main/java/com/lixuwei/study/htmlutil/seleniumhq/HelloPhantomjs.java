package com.lixuwei.study.htmlutil.seleniumhq;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;

public class HelloPhantomjs {

    public static void main(String[] args) throws InterruptedException {
        File file = new File("E:\\headless\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
//        System.setProperty("phantomjs.binary.path", file.getAbsolutePath());

        DesiredCapabilities caps = new DesiredCapabilities();
//        DesiredCapabilities caps = DesiredCapabilities.iphone();
        caps.setJavascriptEnabled(true);
        caps.setCapability("phantomjs.binary.path",
                file.getAbsolutePath());
        String agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1";
        caps.setCapability("phantomjs.page.settings.userAgent", agent);
        WebDriver driver = null;
        try {
//            driver = new PhantomJSDriver(caps);
            driver.manage().window().setSize(new Dimension(375, 667));
            String url = "http://mobile.yangkeduo.com";
            driver.get(url);
            Thread.sleep(5000);
            takeScreenshot(driver, "phantomjs.png");
            String url2 = "http://mobile.yangkeduo.com/mall_page.html?mall_id=805812660&item_index=0&sp=0";
            driver.get(url2);
            Thread.sleep(5000);
            takeScreenshot(driver, "phantomjs2.png");
        } finally {
            if (driver != null) {
                driver.quit();
            }
        }
    }
    public static void takeScreenshot(WebDriver webDriver, String fileName) {
        File screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
