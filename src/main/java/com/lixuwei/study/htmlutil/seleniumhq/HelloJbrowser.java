package com.lixuwei.study.htmlutil.seleniumhq;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;

public class HelloJbrowser {

    public static void main(String[] args) {
        JBrowserDriver driver = new JBrowserDriver(Settings.builder().
                timezone(Timezone.ASIA_SHANGHAI).build());

        // This will block for the page load and any
        // associated AJAX requests
        driver.get("http://mobile.yangkeduo.com/mall_page.html?mall_id=805812660&item_index=0&sp=0");

        // You can get status code unlike other Selenium drivers.
        // It blocks for AJAX requests and page loads after clicks
        // and keyboard events.
        System.out.println(driver.getStatusCode());
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Returns the page source in its current state, including
        // any DOM updates that occurred after page load
        System.out.println(driver.findElementByXPath("//*[@id=\"mp-mgl-0-0-0\"]/div"));

        // Close the browser. Allows this thread to terminate.
        driver.quit();
    }
}
